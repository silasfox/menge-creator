use Data;

class Displayer {
    has @.bible;
    has $!pager = Proc::Async.new(:w, 'less');

    method display {
        @!bible.print;
    }
}
