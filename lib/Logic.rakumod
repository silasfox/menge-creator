use Data;

class Finder {
    has $.bible;
    has $.book;
    has $.chapter;
    has $!verse;

    method find-book(Regex $book) {
        $!book = $!bible.grep({ .name ~~ $book})[0];
        return self;
    }

    method find-chapter($chapter) {
        $!chapter = $!book.chapters[$chapter-1];
        return self;
    }

    method find-verse($verse) {
        $!verse = $!chapter.verses[$verse-1];
        return self;
    }

    method find($request) {
        X::NYI.new.throw;
    }
}

sub semantic-search(@bible, $word, $frequency, $depth) {
    return () if $depth == 0;
    return @bible.grep(False);
}