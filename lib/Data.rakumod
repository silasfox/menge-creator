use XML;

class Book {
    has $.name;
    has $.sname;
    has $.number;

    method from-xml(XML::Element $xml) {
        $!name = $xml<bname>;
        $!sname = $xml<bsname>;
        $!number = $xml<bnumber>;
        return self;
    }

    method Test {
        return "$!name,$!sname,$!number";
    }
}

class Chapter {
    has $.number;
    has Book $.book;

    method from-xml(XML::Element $xml) {
        $!number = $xml<cnumber>;
        return self;
    }

    method Test {
        return "$!number";
    }
}

class Verse is export {
    has Book $.book;
    has Chapter $.chapter;
    has $!number;
    has $!text;

    method from-xml(XML::Element $xml) {
        $!number = $xml<vnumber>;
        $!text = $xml.firstChild.text;
        return self;
    }

    method Test {
        return "$!number,$!text";
    }

    method Str {
        return "{$!book.name}\t{$!book.sname}\t{$!book.number}\t{$!chapter.number}\t{$!number}\t{$!text}\n";
    }
}

class Bible {
    has XML::Element $.xml;
    has @!bible = $!xml.elements.grep({ $_.name ~~ 'BIBLEBOOK' }).map(-> $book { self!book-from-xml($book) });

    method get-bible {
        return @!bible;
    }

    method !verse-from-xml(XML::Element $verse, Book $book, Chapter $chapter) {
        return Verse.new(
                :$book,
                :$chapter).from-xml: $verse;
    }

    method !chapter-from-xml(XML::Element $chapter, Book $book) {
        return self!filter-verses($chapter.elements).map: -> $verse {
            self!verse-from-xml($verse, $book, Chapter.new.from-xml: $chapter).Slip
        };
    }

    method !filter-verses(@chapter-elements) {
        return @chapter-elements.grep({.name ~~ 'VERS'});
    }

    method !book-from-xml(XML::Element $book) {
        return $book.elements.map({
            self!chapter-from-xml($_, Book.new.from-xml: $book).Slip
        }).Slip;
    }
}
